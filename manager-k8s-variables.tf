variable "compute_ec2_manager_server_k8s_subnet" {
  type        = string
  description = "A subnet da instância."
}

variable "compute_ec2_manager_server_k8s_name" {
  type        = string
  description = "Nome do manager server do Kubernetes.s"
}

variable "compute_ec2_manager_server_k8s_ssh_inbound_rules" {
  type = list(object({
    name        = string
    cidr_blocks = list(string)
  }))
  description = "Lista de regras ingress para liberar no security group. name = descrição da regra cidr_blocks = lista de CIDRs para liberar na regra."
}

variable "compute_ec2_manager_server_k8s_termination_protection" {
  type        = bool
  description = "Habilita a proteção contra deleção da EC2 manager_server_k8s."
}


variable "application_eks_cluster_name" {
  type        = string
  description = "Nome do cluster EKS."
}


