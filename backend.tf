terraform {
  backend "s3" {
    bucket         = "nome_do_projeto-ambiente-terraform-states-bucket"
    key            = "infraestrutura/opensearch.tfstate"
    region         = "us-east-1"
    dynamodb_table = "nome_do_projeto-ambiente-terraform-state-lock-table"
    profile        = "nome_do_projeto_ambiente"
  }
} 
