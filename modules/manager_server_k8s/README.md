<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.0.3 |
| aws | >= 2.68 |

## Providers

| Name | Version |
|------|---------|
| aws | 3.55.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| ec2 | ../ec2/ | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.manager](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.manager_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.manager](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.manager_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.manager](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.manager_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| cluster\_name | Nome do cluster EKS. | `string` | n/a | yes |
| compute\_ec2\_manager\_server\_k8s\_name | Nome do manager server do Kubernetes.s | `string` | n/a | yes |
| compute\_ec2\_manager\_server\_k8s\_ssh\_inbound\_rules | Libera IP SG | <pre>list(object({<br>    name        = string<br>    cidr_blocks = list(string)<br>  }))</pre> | n/a | yes |
| compute\_ec2\_manager\_server\_k8s\_subnet | A subnet da instância. | `string` | n/a | yes |
| compute\_ec2\_manager\_server\_k8s\_termination\_protection | Habilita a proteção contra deleção da EC2 manager\_server\_k8s. | `bool` | n/a | yes |
| environment | Ambiente do´projeto | `string` | n/a | yes |
| compute\_ec2\_manager\_server\_k8s\_default\_userdata | Utiliza o userdata default para o bastion. | `bool` | `false` | no |
| helm2\_version | Versão do helm2 | `string` | `"v2.17.0"` | no |
| helm\_version | Versão do helm3 | `string` | `"v3.6.0"` | no |
| kubectl\_latest | Define se a última versão do kubectl será instalada. | `bool` | `true` | no |
| kubectl\_version | Versão específica para ser baixada do kubectl. Funciona apenas se kubectl\_latest for false. | `string` | `"v1.20.0"` | no |
| manager\_permissions | Permissões adicionais para o manager. | `list(string)` | <pre>[<br>  "eks:DescribeCluster",<br>  "eks:UpdateClusterConfig",<br>  "iam:ListOpenIDConnectProviders",<br>  "iam:CreatePolicy"<br>]</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| compute\_ec2\_manager\_server\_k8s\_iam\_role\_arn | ARN da IAM role utilizada pelo manager server do k8s. |
| compute\_ec2\_manager\_server\_k8s\_sg\_id | ID do security group do manager server do k8s. |
<!-- END_TF_DOCS -->
