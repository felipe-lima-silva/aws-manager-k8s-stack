variable "compute_ec2_manager_server_k8s_subnet" {
  type        = string
  description = "A subnet da instância."
}
variable "environment" {
  type        = string
  description = "Ambiente do´projeto"
}

variable "compute_ec2_manager_server_k8s_name" {
  type        = string
  description = "Nome do manager server do Kubernetes.s"
}

variable "compute_ec2_manager_server_k8s_ssh_inbound_rules" {
  type = list(object({
    name        = string
    cidr_blocks = list(string)
  }))
  description = "Libera IP SG"
}

variable "compute_ec2_manager_server_k8s_termination_protection" {
  type        = bool
  description = "Habilita a proteção contra deleção da EC2 manager_server_k8s."
}


variable "compute_ec2_manager_server_k8s_default_userdata" {
  type        = bool
  description = "Utiliza o userdata default para o bastion."
  default     = false
}

variable "kubectl_latest" {

  type        = bool
  default     = true
  description = "Define se a última versão do kubectl será instalada."

}

variable "kubectl_version" {

  type        = string
  default     = "v1.20.0"
  description = "Versão específica para ser baixada do kubectl. Funciona apenas se kubectl_latest for false."
}


variable "cluster_name" {

  type        = string
  description = "Nome do cluster EKS."
}


variable "manager_permissions" {

  type        = list(string)
  default     = ["eks:DescribeCluster", "eks:UpdateClusterConfig", "iam:ListOpenIDConnectProviders", "iam:CreatePolicy"]
  description = "Permissões adicionais para o manager."
}

variable "helm_version" {
  type        = string
  description = "Versão do helm3"
  default     = "v3.6.0"
}

variable "helm2_version" {
  type        = string
  description = "Versão do helm2"
  default     = "v2.17.0"
}
