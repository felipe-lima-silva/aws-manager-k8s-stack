#!/bin/bash
export USER=ec2-user
# desc: Este script é usado para configurar a parte essencial em um CentOS 7

# Pacotes essenciais
sudo yum install mysql -y
sudo yum install iftop -y
sudo yum install htop -y
sudo yum install psmisc -y
sudo yum install telnet -y
sudo yum install ntp -y

# Configurando ssh
sudo sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sudo sed -i 's/PasswordAuthentication no/#PasswordAuthentication no/g' /etc/ssh/sshd_config
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo sed -i 's/#Port 22/Port 35222/g' /etc/ssh/sshd_config; service sshd restart
sudo cat /home/$USER/.ssh/authorized_keys  > /root/.ssh/authorized_keys 

# Configurando SELinux
sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config 

# Instalando Epel (caso ainda não tenha sido instalado)
sudo yum install -y epel-release

# Atualizando pacotes
sudo yum update -y

# Preparando o ambiente
sudo mkdir /teste ; mkdir /scripts ; mkdir /download 
sudo echo 'alias l="ls -la --color"' >> /etc/profile
sudo echo 'PATH=$PATH:/scripts/:~/.local/bin' >> /etc/profile

# Desativando serviços
sudo service firewalld stop
sudo systemctl disable firewalld.service
sudo systemctl disable postfix.service
sudo systemctl disable  cloud-config.service cloud-final.service cloud-init-local.service cloud-init.service

# Instalando o AWS Cli
sudo yum install -y awscli

# Alterando Limits de arquivos abertos
sudo sed -i 's/4096/307200/g' /etc/security/limits.d/20-nproc.conf
sudo echo '* hard nofile 500000' >> /etc/security/limits.conf
sudo echo '* soft nofile 500000' >> /etc/security/limits.conf
sudo echo 'root hard nofile 500000' >> /etc/security/limits.conf
sudo echo 'root soft nofile 500000' >> /etc/security/limits.conf
sudo echo 'fs.file-max = 2097152' >> /etc/sysctl.conf
sudo sleep 1; sysctl -p

# Timezone de Sao Paulo
sudo rm -f /etc/localtime
sudo ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime


# Instalando o aws-iam-authenticator
aws-iam-authenticator help
if [[ $? -ne 0 ]]; then
	sudo curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.18.9/2020-11-02/bin/linux/amd64/aws-iam-authenticator
        sudo chmod +x ./aws-iam-authenticator
	sudo mkdir -p $HOME/bin && sudo cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$PATH:$HOME/bin
	sudo echo 'export PATH=$PATH:$HOME/bin' >> ~/.bash_profile
	sudo mv bin/aws-iam-authenticator /usr/local/bin/
else
	echo aws-iam-authenticator já instalado
fi;

# Instalando o kubectl
kubectl
if [[ $? -ne 0 ]]; then
	    curl -LO ${kubectl_url}
	    chmod +x ./kubectl
	    sudo mv ./kubectl /usr/bin/kubectl
            echo $(kubectl version --client)
	    else echo "kubectl já instalado."
fi;


# Instalando o Helm
runuser -u $USER -- aws eks --region ${region} update-kubeconfig --name ${cluster_name}
runuser -u $USER -- curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh && chmod 700 get_helm.sh && DESIRED_VERSION=${helm_version} ./get_helm.sh
runuser -u $USER -- sudo rm get_helm.sh
runuser -u $USER -- sudo mv /usr/local/bin/helm /usr/local/bin/helm3
runuser -u $USER -- helm3 repo add stable https://charts.helm.sh/stable
runuser -u $USER -- curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh && chmod 700 get_helm.sh && DESIRED_VERSION=${helm2_version} ./get_helm.sh
runuser -u $USER -- rm get_helm.sh
runuser -u $USER -- kubectl apply -f - <<EOF
${helm_2_tiller}
EOF
runuser -u $USER -- helm init --service-account tiller --tiller-namespace kube-system

# Instalando o auto complete do kubectl
runuser -u $USER -- echo 'source <(kubectl completion bash)' >> /home/$USER/.bashrc
kubectl completion bash >/etc/bash_completion.d/kubectl
echo 'alias k=kubectl' >>/home/$USER/.bashrc
echo 'complete -F __start_kubectl k' >>/home/$USER/.bashrc
runuser -u $USER -- bash

# Instalando o metrics server
runuser -u $USER -- kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.7/components.yaml
