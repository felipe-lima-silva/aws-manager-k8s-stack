output "compute_ec2_manager_server_k8s_sg_id" {
  value       = module.ec2.compute_ec2_bastion_sg_id
  description = "ID do security group do manager server do k8s."
}

output "compute_ec2_manager_server_k8s_iam_role_arn" {
  value       = aws_iam_role.manager.arn
  description = "ARN da IAM role utilizada pelo manager server do k8s."
}
