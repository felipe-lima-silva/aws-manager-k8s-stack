locals {
  kubectl_version = var.kubectl_version
  latest          = "$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)"
  kubectl_url     = "https://storage.googleapis.com/kubernetes-release/release/${var.kubectl_latest ? local.latest : local.kubectl_version}/bin/linux/amd64/kubectl"

}

module "ec2" {

  source                                          = "../ec2/"
  compute_ec2_bastion_name                        = var.compute_ec2_manager_server_k8s_name
  compute_ec2_bastion_subnet                      = var.compute_ec2_manager_server_k8s_subnet
  compute_ec2_bastion_ssh_inbound_rules           = var.compute_ec2_manager_server_k8s_ssh_inbound_rules
  compute_ec2_bastion_termination_protection      = var.compute_ec2_manager_server_k8s_termination_protection
  compute_ec2_bastion_existing_userdata_path_file = templatefile("${path.module}/user_data.sh", { kubectl_url = local.kubectl_url, helm_version = var.helm_version, helm2_version = var.helm2_version, region = data.aws_region.current.name, cluster_name = var.cluster_name, helm_2_tiller = templatefile("${path.module}/helm_2_tiller.yaml", {}) })
  compute_ec2_bastion_default_userdata            = var.compute_ec2_manager_server_k8s_default_userdata
  compute_ec2_bastion_iam_instance_profile        = aws_iam_instance_profile.manager.name
  compute_ec2_bastion_ssh_default_key             = false
  compute_ec2_bastion_ssh_key_path                = templatefile("${path.module}/ec2_manager_k8s_${var.environment}.pub", {})
}

resource "aws_iam_role" "manager" {
  name = "EC2-Role-${var.compute_ec2_manager_server_k8s_name}"
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.manager.json
}


data "aws_iam_policy_document" "manager" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}



resource "aws_iam_instance_profile" "manager" {
  name = "EC2-Role-${var.compute_ec2_manager_server_k8s_name}"
  role = aws_iam_role.manager.name
}

data "aws_iam_policy_document" "manager_policy" {
  count = length(var.manager_permissions) > 0 ? 1 : 0
  statement {
    actions   = var.manager_permissions
    resources = ["*"]
  }
  statement {
    actions   = ["sts:AssumeRole"]
    resources = [aws_iam_role.manager.arn]
  }
}




resource "aws_iam_policy" "manager_policy" {
  count  = length(var.manager_permissions) > 0 ? 1 : 0
  name   = join("-", ["EC2-Policy-${var.compute_ec2_manager_server_k8s_name}-EKS-Cluster", var.cluster_name])
  path   = "/"
  policy = data.aws_iam_policy_document.manager_policy[0].json
}



resource "aws_iam_role_policy_attachment" "manager_policy" {
  count      = length(var.manager_permissions) > 0 ? 1 : 0
  role       = aws_iam_role.manager.name
  policy_arn = aws_iam_policy.manager_policy[0].arn
}
