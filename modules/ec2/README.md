<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.0.3 |
| aws | >= 2.68 |

## Providers

| Name | Version |
|------|---------|
| aws | 3.55.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eip.bastion](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_eip_association.bastion](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip_association) | resource |
| [aws_instance.bastion](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_key_pair.bastion](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |
| [aws_security_group.bastion_ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.bastion_ec2_remote_ssh](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_ami.amazon_linux](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_subnet.bastion](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| compute\_ec2\_bastion\_default\_userdata | Utiliza o userdata default para o bastion. | `bool` | n/a | yes |
| compute\_ec2\_bastion\_existing\_userdata\_path\_file | Conteúdo de userdata existente. Utilize a função template(). | `string` | n/a | yes |
| compute\_ec2\_bastion\_name | Nome do bastion | `string` | n/a | yes |
| compute\_ec2\_bastion\_ssh\_default\_key | Define se a instância usará a key pair SSH default. | `bool` | n/a | yes |
| compute\_ec2\_bastion\_ssh\_inbound\_rules | Lista de regras ingress para liberar no security group. name = descrição da regra cidr\_blocks = lista de CIDRs para liberar na regra. | <pre>list(object({<br>    name        = string<br>    cidr_blocks = list(string)<br>  }))</pre> | n/a | yes |
| compute\_ec2\_bastion\_subnet | A subnet da instancia | `string` | n/a | yes |
| compute\_ec2\_bastion\_termination\_protection | Habilita a proteção contra deleção da EC2 bastion. | `bool` | n/a | yes |
| compute\_ec2\_bastion\_iam\_instance\_profile | Nome do instance profile da EC2. Opcional. | `string` | `""` | no |
| compute\_ec2\_bastion\_ssh\_key\_path | Path do arquivo de uma chave pública SSH. Obrigatório quando **compute\_ec2\_bastion\_ssh\_default\_key** for false. | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| compute\_ec2\_bastion\_sg\_id | ID do security group do Bastion |
<!-- END_TF_DOCS -->
