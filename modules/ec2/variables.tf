variable "compute_ec2_bastion_subnet" {
  type        = string
  description = "A subnet da instancia"
}

variable "compute_ec2_bastion_name" {
  type        = string
  description = "Nome do bastion"
}

variable "compute_ec2_bastion_ssh_inbound_rules" {
  type = list(object({
    name        = string
    cidr_blocks = list(string)
  }))
  description = "Lista de regras ingress para liberar no security group. name = descrição da regra cidr_blocks = lista de CIDRs para liberar na regra."
}

variable "compute_ec2_bastion_termination_protection" {
  type        = bool
  description = "Habilita a proteção contra deleção da EC2 bastion."
}

variable "compute_ec2_bastion_default_userdata" {
  type        = bool
  description = "Utiliza o userdata default para o bastion."
}

variable "compute_ec2_bastion_existing_userdata_path_file" {
  type        = string
  description = "Conteúdo de userdata existente. Utilize a função template()."
}

variable "compute_ec2_bastion_iam_instance_profile" {
  type        = string
  description = "Nome do instance profile da EC2. Opcional."
  default     = ""
}

variable "compute_ec2_bastion_ssh_default_key" {
  type        = bool
  description = "Define se a instância usará a key pair SSH default."
}

variable "compute_ec2_bastion_ssh_key_path" {
  type        = string
  description = "Path do arquivo de uma chave pública SSH. Obrigatório quando **compute_ec2_bastion_ssh_default_key** for false."
  default     = ""
}
