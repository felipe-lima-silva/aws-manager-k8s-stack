data "aws_subnet" "bastion" {
  id = var.compute_ec2_bastion_subnet
}

data "aws_ami" "amazon_linux" {

  most_recent = true
  owners      = ["amazon"]
  filter {
    name = "name"
    values = [
      #      "amzn2-ami-hvm-2.0.20210721.2-x86_64-gp2",
      "amzn2-ami-hvm-2.0.20210813.1-x86_64-gp2"
    ]
  }
  filter {
    name = "owner-alias"
    values = [
      "amazon",
    ]
  }
}
