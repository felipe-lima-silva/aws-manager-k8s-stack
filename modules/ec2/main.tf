resource "aws_key_pair" "bastion" {
  key_name   = "${var.compute_ec2_bastion_name}-Key"
  public_key = var.compute_ec2_bastion_ssh_key_path
  tags = {
    Name = var.compute_ec2_bastion_name
  }
}
resource "aws_eip" "bastion" {
  instance = aws_instance.bastion.id
  vpc      = true
  tags     = { Name = var.compute_ec2_bastion_name }
}


resource "aws_eip_association" "bastion" {
  instance_id   = aws_instance.bastion.id
  allocation_id = aws_eip.bastion.id
}


resource "aws_instance" "bastion" {
  ami                                  = data.aws_ami.amazon_linux.id
  instance_type                        = "t3.small"
  subnet_id                            = var.compute_ec2_bastion_subnet
  associate_public_ip_address          = false
  iam_instance_profile                 = var.compute_ec2_bastion_iam_instance_profile
  vpc_security_group_ids               = [aws_security_group.bastion_ec2.id]
  key_name                             = aws_key_pair.bastion.id
  user_data                            = var.compute_ec2_bastion_default_userdata ? templatefile("${path.module}/user_data.sh", {}) : var.compute_ec2_bastion_existing_userdata_path_file
  disable_api_termination              = var.compute_ec2_bastion_termination_protection
  instance_initiated_shutdown_behavior = "stop"
  tags = {
    Name = var.compute_ec2_bastion_name
  }
}

resource "aws_security_group" "bastion_ec2" {
  name                   = "SG-EC2-${var.compute_ec2_bastion_name}"
  description            = "SG-EC2-${var.compute_ec2_bastion_name}"
  vpc_id                 = data.aws_subnet.bastion.vpc_id
  revoke_rules_on_delete = true

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SG-EC2-${var.compute_ec2_bastion_name}"
  }
}

resource "aws_security_group_rule" "bastion_ec2_remote_ssh" {
  count             = length(var.compute_ec2_bastion_ssh_inbound_rules)
  type              = "ingress"
  from_port         = 35222
  to_port           = 35222
  protocol          = "tcp"
  security_group_id = aws_security_group.bastion_ec2.id
  description       = lookup(var.compute_ec2_bastion_ssh_inbound_rules[count.index], "name", "")
  cidr_blocks       = lookup(var.compute_ec2_bastion_ssh_inbound_rules[count.index], "cidr_blocks", "")
}
