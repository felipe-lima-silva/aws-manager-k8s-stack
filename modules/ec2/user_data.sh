#!/bin/bash

# desc: Este script é usado para configurar a parte essencial em um CentOS 7

# Pacotes essenciais
sudo yum install mysql -y
sudo yum install iftop -y
sudo yum install htop -y
sudo yum install psmisc -y
sudo yum install telnet -y
sudo yum install ntp -y

# Configurando ssh
sudo sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sudo sed -i 's/PasswordAuthentication no/#PasswordAuthentication no/g' /etc/ssh/sshd_config
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo sed -i 's/#Port 22/Port 35222/g' /etc/ssh/sshd_config; service sshd restart
sudo cat /home/ec2-user/.ssh/authorized_keys  > /root/.ssh/authorized_keys 

# Configurando SELinux
sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config 

# Instalando Epel (caso ainda não tenha sido instalado)
sudo yum install -y epel-release

# Atualizando pacotes
sudo yum update -y

# Preparando o ambiente
sudo mkdir /teste ; mkdir /scripts ; mkdir /download 
sudo echo 'alias l="ls -la --color"' >> /etc/profile
sudo echo 'PATH=$PATH:/scripts/:~/.local/bin' >> /etc/profile

# Desativando serviços
sudo service firewalld stop
sudo systemctl disable firewalld.service
sudo systemctl disable postfix.service
sudo systemctl disable  cloud-config.service cloud-final.service cloud-init-local.service cloud-init.service

# Instalando o AWS Cli
sudo yum install -y awscli

# Alterando Limits de arquivos abertos
sudo sed -i 's/4096/307200/g' /etc/security/limits.d/20-nproc.conf
sudo echo '* hard nofile 500000' >> /etc/security/limits.conf
sudo echo '* soft nofile 500000' >> /etc/security/limits.conf
sudo echo 'root hard nofile 500000' >> /etc/security/limits.conf
sudo echo 'root soft nofile 500000' >> /etc/security/limits.conf
sudo echo 'fs.file-max = 2097152' >> /etc/sysctl.conf
sudo sleep 1; sysctl -p

# Timezone de Sao Paulo
sudo rm -f /etc/localtime
sudo ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
