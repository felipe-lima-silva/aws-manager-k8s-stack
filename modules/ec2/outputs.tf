output "compute_ec2_bastion_sg_id" {
  value       = aws_security_group.bastion_ec2.id
  description = "ID do security group do Bastion"
}
