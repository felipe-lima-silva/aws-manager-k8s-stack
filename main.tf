module "ec2_manager_server_k8s" {
  source                                                = "./modules/manager_server_k8s/"
  compute_ec2_manager_server_k8s_name                   = var.compute_ec2_manager_server_k8s_name
  compute_ec2_manager_server_k8s_subnet                 = var.compute_ec2_manager_server_k8s_subnet
  compute_ec2_manager_server_k8s_ssh_inbound_rules      = var.compute_ec2_manager_server_k8s_ssh_inbound_rules
  compute_ec2_manager_server_k8s_termination_protection = var.compute_ec2_manager_server_k8s_termination_protection
  compute_ec2_manager_server_k8s_default_userdata       = false
  cluster_name                                          = var.application_eks_cluster_name
  environment                                           = var.environment
}
