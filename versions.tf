terraform {
  required_version = ">= 1.0.3"
  required_providers {
    aws        = ">= 2.68"
    kubernetes = ">= 1.11.1"
  }
}
