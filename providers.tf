provider "aws" {
  region              = var.region
  profile             = "${var.profile_preffix}_${var.environment}"
  allowed_account_ids = [var.account_id]


  default_tags {
    tags = var.tags

  }
}
