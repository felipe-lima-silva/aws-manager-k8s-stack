compute_ec2_manager_server_k8s_ssh_inbound_rules = [{
  name        = "Terraform EC2"
  cidr_blocks = ["0.0.0.0/0"]
}]
region                                                = "us-east-1"
compute_ec2_manager_server_k8s_name                   = "projeto-k8s-manager"
application_eks_cluster_name                          = "projeto-ambiente"
compute_ec2_manager_server_k8s_termination_protection = true
compute_ec2_manager_server_k8s_subnet                 = "XXXXXXXXXXXXX"
